// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .



    <script type="text/javascript">

    $(document).ready(function(){
      $("#myCarousel").carousel();
          $(window).bind('scroll',function(e){
   		     parallaxScroll();
   	
   	});
 
   	function parallaxScroll(){
   		var scrolledY = $(window).scrollTop(10)
   		$('#myCarousel').css('top','-'+((scrolledY*2.3))+'px');
   		$('.loremipsum').css('background-position', 'center', '-'+((scrolledY*0.2))+'px');
		$('.faces').css('top','-'+((scrolledY*1.2))+'px');
	   }
    });

    </script>

